# -*- mode: tqfolder -*-


#<style.default.title = "Signal", style.default.histLineColor = kBlue, style.default.histLineWidth = 2, style.default.histMarkerStyle = 20, style.default.histMarkerSize = 1.2> @ /sig/?/?/vh;

<style.default.title = "$WZ$", style.default.histLineColor = kRed, style.default.histFillColor = kRed> @ /bkg/?/?/prompt/diboson/WZ/;
<style.default.title = "$t\bar{t}$", style.default.histLineColor = kOrange, style.default.histFillColor = kOrange> @ /bkg/?/?/non_prompt/top/ttbar/;
<style.default.title = "$ZZ$", style.default.histLineColor = kGreen, style.default.histFillColor = kGreen> @ /bkg/?/?/prompt/diboson/ZZ/;
<style.default.title = "$tW$", style.default.histLineColor = kMagenta, style.default.histFillColor = kMagenta> @ /bkg/?/?/non_prompt/top/tW/;
#<style.default.title = "$Zee$", style.default.histLineColor = kMagenta+3, style.default.histFillColor = kMagenta+3> @ /bkg/?/?/non_prompt/Vjets/Zjets/Zee/;
#<style.default.title = "$Zmumu$", style.default.histLineColor = kMagenta-5, style.default.histFillColor = kMagenta-5> @ /bkg/?/?/non_prompt/Vjets/Zjets/Zmumu/;
#<style.default.title = "$tWZ$", style.default.histLineColor = kMagenta, style.default.histFillColor = kMagenta> @ /bkg/?/?/prompt/top/tWZ/;
<style.default.title = "$data$", style.default.isHistogram = false, style.default.histLineColor = 1, style.default.histMarkerStyle = 20, style.default.histMarkerSize = 1.2, style.default.showRaw = true> @ data/, data/?/?/;
