MVAWeights
=========================

`CAFCore` supports multivariate analyses by utilizing `TMVA`, which is
the implementation of multivariate analyses native to `ROOT`. `TMVA`
stores trained classifiers such as BDTs or ANNs in the form of
*weights* files. These are text files that utilize an `xml`-style
syntax (even though not all of the `xml` specifications are
followed). Since training these classifiers is a lot of work, and a
trained classifier is the key component to a multivariate analysis,
this folder allows you to store the (machine generated) *weights*
files of your multivariate analysis.

Usage
--------------------

In any place you would usually use a branch or observable name, you
can also use the output of a multivariate classifier by simply
providing the path to the corresponding *weights* file.

For example, in your histograms file, you can write

    TH1F('BDT, '', 10, 0., 1.) << (common/auxData/MVAWeights/myBDT_weights.xml : 'BDT'); 
    
to have a histogram filled with your `BDT` output. You can of course
also cut on your `BDT` output by using this in your cut definition
file like this:

      +CutBDT { 
        <.cutExpression="[common/auxData/MVAWeights/myBDT_weights.xml] > 0.5">
      }
      
Note how the file path is here encapsulated with brackets `[...]` to
force the framework to create an instance of `TQMVAObservable` here
and pass its output further down the chain. If you leave out the
brackets, the framework will mistaken your file path for the name of a
branch in this situation and your analysis will crash with an error
message complaining that initialization of a `TQTreeFormulaObservable`
failed.  Whenever you want to combine the output of your MVA with any
additional operation (like a multiplication, addition, or comparison
with another value, fixed or otherwise), you'll need to use brackets
`[...]` to separate your *weights* file path from the rest of your
expression.

But even if you do this, things go wrong sometimes. The way the
framework identifies when to use a `TQMVAObservable` is simple: when
an expression ends with `weights.xml`, it assumes that this is the
path of a *weights* file. If your file is named differently, you can
force the framework to interpret your expression as a *weights* file
by prepending `TMVA:` to your path.

      
Additional Information
--------------------

`CAFCore` needs some additional information that is not (by default)
provided by `TMVA` when writing out classifiers, namely how the input
variables of the MVA actually map to the branches in your samples by
means of observables). The `CAFCore` wrapper `TQMVA` thus edits the
*weights* files slightly to add this information. Thus, you will in
general not be able to use *weights* files generated by a standalone
analysis code using `TMVA` inside your `CAFCore` analysis, unless you
edit them by hand. In order to do this, you need to modify the
`Variables` block such that each `Variable` line contains the
expression identifying your input variable in the `Label` keyword,
like in this example:

    <Variables NVar="...">
      <Variable VarIndex="0" Expression="MT" Label="Event$(cand)[0].mt(0,1)/1000." Title="M_{#it{T}}" Unit="" Internal="MT" Type="F" Min="10" Max="150"/>
      ...
    </Variables>
    
If your *weights* file was generated by `CAFCore`, this should already
be the case.


