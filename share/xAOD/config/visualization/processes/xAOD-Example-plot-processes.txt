.path = "bkg/$(channel)/$(campaign)/Zjets_Powheg",         .name = "ZjetsPowheg",   .title = "$Z+$jets",   .isSignal = true, histLineColor = kGreen, histLineWidth = 3, histLineStyle = 1, histFillStyle = 0, stack = true
.path = "bkg/$(channel)/$(campaign)/Vgamma",                       .name = "Vgamma", .title = "$V+\#gamma$", .isBackground = true, histFillColor = 419, histLineColor = kBlack
# .path = "bkg/$(channel)/$(campaign)/diboson/WW",                   .name = "WW",     .title = "$WW$",        .isBackground = true, histFillColor = 594, histLineColor = kBlack
.path = "bkg/$(channel)/$(campaign)/diboson/NonWW/WZgammaStar",    .name = "WZ",     .title = "$WZ$",        .isBackground = true, histFillColor = kOrange+8, histLineColor = kBlack
.path = "bkg/$(channel)/$(campaign)/diboson/NonWW/ZZ",      .name = "ZZ",     .title = "$ZZ$",        .isBackground = true, histFillColor = 610, histLineColor = kBlack
.path = "bkg/$(channel)/$(campaign)/top",                   .name = "top", .title = "Top", .isBackground = true, histFillColor = 219, histLineColor = kBlack
#.path = "bkg/$(channel)/$(campaign)/triboson",                     .name = "VVV",    .title = "$VVV$",       .isBackground = true, histFillColor = kGray, histLineColor = kBlack
.path = "data/$(channel)/$(campaign)",                             .name = "data",   .title = "Data",        .isData = true, histMarkerColor = kBlack, histMarkerSize = 1, histMarkerStyle = 20, histLineColor = kBlack, histLineWidth = 3
# .path = "data/$(channel)/$(campaign)-bkg/$(channel)/$(campaign)/[Vgamma+diboson+top+triboson]",  .name = "fakes", .title = "fakes", .isData = false, .isSignal = true, stack=false, drawOptions = "ep", histMarkerColor = kBlue, histMarkerSize = 1, histMarkerStyle = 27, histLineColor = kBlue, histLineWidth = 3
